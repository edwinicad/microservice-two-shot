# Wardrobify

Team:

* Person 1 - Which microservice?
Nickie - Hats
* Person 2 - Which microservice?
Zach - Shoes

How to Run this App
    This app is designed to allow you to keep track of your shoes and hats.
    To add a hat or shoe, click on the add button
    To delete a hat or shoe, click on the delete button
    To show a list of hats, click on the list hats button
    To show a list of shoes, click on the list shoes button

## Design
Wardrobe continually pulls data from the ShoeModel and Hat microservices, and is available upon demand.
When a client makes a request based on the input at the website, that data is displayed. The request can be either to list clothing, or to add or delete an article of clothing. The request is patched through the appropriate URL to the main project microservice, which is called Wardrobe; Wardrobe invokes the necessary function with a view function and returns the request response via the webiste. If the request is to create or delete, the request is patched through the Hats or Shoes bounded context until it makes an update_or_delete request. Once this happens, the next poll should contain the updated data.

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The ShoeModel creates instances of shoes with the following properties:
    -manufacturer
    -model name
    -color
    -a url link to a picture of the shoe
    -the bin number and closet number where the shoe is

The ShoeModelEncoder creates an instance of a shoe item with:
    -a url link to a picture of the model
    -a bin location where the shoe is kept
    -the size of the bin
    -the closet name where the shoe is kept
    -an id number for the shoe

The Bin Value Object is a model that is used to periodically pull details regarding the contents of the wardrobe.
This is important because with those details, the website can display accurate, up-to-date information about which shoes are where.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

My project includes two models: hats and locations. With the foreign key, the hat corresponds to a certain location. The location model establishes the location within the wardrobe using the closet name, section number, and shelf, while the hat depicts the different hats in the wardrobe. My view function makes a request to the microservice and then returns to create the objects in the app.
