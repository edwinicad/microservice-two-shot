import React, {useState, useEffect } from 'react';



function HatForm() {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
      style: '',
      fabric: '',
      color: '',
      location: ''
    });


    const getData = async () => {
        const url = 'http://localhost:8090/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
      }

    useEffect(() => {
        getData();
      }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8090/api/hats/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          };

    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto"></div>
                    <h1>Create a new hat</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form"></form>
            </div>
        </div>
    );
}

export default HatForm;
