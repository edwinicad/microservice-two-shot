import React, {useState, useEffect} from 'react'


function HatsList(){
    const [hats, setHats] = useState([])

    const getData = async () =>{
        const response = await fetch("http://localhost:8090/api/hats/");

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect ()

    return (
        <table className='"table table-striped'>
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Wardrobe</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(attendee => {
                    <tr key={hats.href}>
                        <td><img src={hats.picture_url} width='100'/></td>
                        <td>{hats.style_name}</td>
                        <td>{hats.fabric}</td>
                        <td>{hats.color}</td>
                        <td>{hats.location.closet_name}</td>
                        <td>
                            <button className="btn btn-primary">Delete</button>
                        </td>
                    </tr>
                })}
            </tbody>
        </table>

    )
}

export default HatsList
