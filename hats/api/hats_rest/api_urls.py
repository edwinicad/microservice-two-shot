from django.urls import path

from .views import  list_hats, hat_detail


urlpatterns = [
    path('hats/', list_hats, name='create_hats'),
    path ('hats/<int:pk>/', hat_detail, name='hat_detail'),
    path('location/<int:location_vo_id>/hats/',list_hats, name="list_hats"),
    #path('locations/', location_detail_view, name='location_detail')
    #path('hats/create', create_hat, name='create_hat'),
    #path('hats/delete/<int:pk>/', delete_hat, name='delete_hat'),
]
