from django.contrib import admin

from .models import Hat, Location





@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = ('style_name', 'fabric', 'color', 'location')

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('import_href', 'closet_name', 'shelf_number', 'section_number')
