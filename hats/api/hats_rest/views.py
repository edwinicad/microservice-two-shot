import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder


from .models import Hat, Location

class LocationVoEncoder(ModelEncoder):
    model=Location
    properties = [
        "id",
    ]

class HatDetailEncoder(ModelEncoder):
    model= Hat
    properties=[
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'location',
    ]
    encoders ={"location": LocationVoEncoder()},


class HatListEncoder(ModelEncoder):
    model= Hat
    properties=[
        "id",
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'location',
    ]
    encoders ={"location": LocationVoEncoder()},






@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == 'GET':
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)

        else:
            hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = Location.objects.get(import_href=location_href)
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def hat_detail(request,pk):
    if request.method == "GET":
        try:
            hat= Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder= HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Location.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
