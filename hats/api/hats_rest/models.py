from django.db import models
from django.urls import reverse

class Location (models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    section_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"


class Hat(models.Model):
    fabric = models.CharField(max_length=200, null=True)
    style_name = models.CharField(max_length=200, null=True,blank=True)
    color = models.CharField(max_length=200,null=True )
    picture_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(
        Location,
        related_name="hats",
        on_delete=models.CASCADE
    )

    # def get_api_url(self):
    #     return reverse("hat_detail", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.style_name}"

    class Meta:
        ordering = ["style_name",]
