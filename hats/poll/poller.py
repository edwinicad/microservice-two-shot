import django
import os
import sys
import time
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()


from hats_rest.models import Location


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            response = requests.get('http://wardrobe-api:8000/api/locations/')
            content = response.json()

            for location in content['locations']:
                Location.objects.update_or_create(import_href=location["href"])
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
