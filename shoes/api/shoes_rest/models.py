from django.db import models
from django.urls import reverse


# since we can't access objects from another microservice, we poll the wardrobe microservice every so often
# this is just to see what is there (getting the data that exists)
# the BinVO is created or updated every time there is a poll


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class ShoeModel(models.Model):
    manufacturer = models.CharField(max_length=200, null=True)
    model_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    href = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoemodels",
        null=True,
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("show_shoe_details", kwargs={"id": self.id})

    def __str(self):
        return self.model_name

    class Meta:
        ordering = ("bin", "model_name") # Default ordering for ShoeModel
