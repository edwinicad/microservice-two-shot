from django.contrib import admin
from .models import ShoeModel, BinVO

# Register your models here.
admin.site.register(ShoeModel)
admin.site.register(BinVO)
