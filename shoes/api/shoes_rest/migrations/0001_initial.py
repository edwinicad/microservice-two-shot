# Generated by Django 4.0.3 on 2024-05-02 20:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=200, unique=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='ShoeModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manufacturer', models.CharField(max_length=200, null=True)),
                ('model_name', models.CharField(max_length=200, null=True)),
                ('color', models.CharField(max_length=200, null=True)),
                ('href', models.URLField(null=True)),
                ('bin', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='shoemodels', to='shoes_rest.binvo')),
            ],
            options={
                'ordering': ('bin', 'model_name'),
            },
        ),
    ]
