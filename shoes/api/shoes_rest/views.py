from django.http import JsonResponse
from .acls import get_photo
from django.views.decorators.http import require_http_methods
import json

# from django.shortcuts import render
from common.json import ModelEncoder
from .models import BinVO, ShoeModel


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]


class ShoeModelEncoder(ModelEncoder):
    model = ShoeModel
    properties = [
        "id",
        "closet_name",
        "bin_number",
        "bin_size",
        "href",
    ]


# Create your views here.
@require_http_methods(["GET", "POST"])
def list_bins(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id == "None":
            shoes_list = ShoeModel.objects.all()
        else:
            shoes_list = ShoeModel.objects.filter(id=bin_vo_id)
        return JsonResponse(
            {"shoes": shoes_list},
            encoder=ShoeModelEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            model = ShoeModel.objects.get(model_name=content[bin_vo_id])
            content["model_name"] = model
        except ShoeModel.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe model"},
                status=400,
            )
        photo_url = get_photo(content["model_name"])
        content.update(photo_url)
        shoe = ShoeModel.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeModelEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def show_shoe_details(request, id):
    pass
#     shoe = ShoeModel
        # try:
        #     bin = Bin.objects.get(id=)

@require_http_methods(["GET", "DELETE", "PUT"])
def show_bin_details(request, id):
    pass
