from django.urls import path
from .views import list_bins, show_shoe_details, show_bin_details

urlpatterns = [
    path('bins/', list_bins, name='list_bins'),
    path('bins/<int:id>/', show_shoe_details, name='show_shoe_details'),
    path(
        'bins/<int:bin_id>/',
        show_bin_details,
        name='show_bin_details'
    ),
]
