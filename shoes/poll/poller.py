import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
from shoes_rest.models import BinVO

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            response = requests.get('http://wardrobe-api:8000/api/bins/')
            content = response.json()

            for bin in content["bins"]:
            #if the bin DNE, create one
                BinVO.objects.update_or_create(
                #this updates or creates with the following content
                    import_href = bin["href"],

                    defaults = {
                        "bin_size": bin["bin_size"],
                        "closet_name": bin["closet_name"],
                        },
                )
                    # "closet name": bin[closet]; this shunts any new bin into the closet it belongs in
                    # ANY necessary properties must be added here
                    # this is only necessary if you have BinVOs that lack properties
                    # shoe models need to know where they live and they can't access or import from without
                    # this is why we need a BinVO model
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
